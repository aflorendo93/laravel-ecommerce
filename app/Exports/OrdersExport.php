<?php

namespace App\Exports;

use App\Order;
// use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Auth;

// class OrdersExport implements FromCollection
// {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     public function collection()
//     {
//         return Order::all();
//     }
// }

class OrdersExport implements FromView
{
    public function view(): View
    {
        $orders = Order::where("user_id", Auth::user()->id)->get();

        return view('orders-csv', compact('orders'));

    }
}
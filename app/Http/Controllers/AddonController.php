<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Exports\OrdersExport;
use Auth;
use PDF;
use Excel;

class AddonController extends Controller
{
    public function downloadOrders(){

        $orders = Order::where("user_id", Auth::user()->id)->get();
        //this line creates the pdf file from the view file
        $pdf = PDF::loadView('user-transaction', compact('orders'));
        $filename = Auth::user()->name . "_".now() . ".pdf";

        return $pdf->download($filename);
        
    }

    public function downloadOrdersCsv(){
        return Excel::download(new OrdersExport, 'orders.xlsx');
    }
}

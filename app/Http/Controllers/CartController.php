<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Session;
use Alert;

class CartController extends Controller
{
    public function addToCart($item_id, Request $request){
        //1. Check if we have enough quantity.
        //get the item
        $item = Item::find($item_id);

        //compare the quantity from db vs the quantity from form
        //check if the quantity received is greater than 0;
        //check if the quantity received + the quantity in session is less thant the quantity in stock
        if($item->quantity < $request->cart_quantity){
            Session::flash("message", "Invalid quantity");
            return back();
        }

      //2. If we have enough quantity, check if we already have a session cart
            //2.1 if we already have, get the cart
            //2.2 if we don't have, set an empty array/
        if(Session::has('cart')){
            $cart = Session::get('cart');
        }else{
            $cart = [];
        } 
        //3. Check if the item in our cart ($item_id) has a quantity already. If yes, add to it, if no, assign.
        if(isset($cart[$item_id])){
            $cart[$item_id] += $request->cart_quantity;
        }else{
            $cart[$item_id] = $request->cart_quantity;
        }

        //4. Put the items back to session->cart.
        Session::put('cart', $cart);
        Session::flash('message', "Successfully added $request->cart_quantity $item->name to cart");
        //5. redirect to catalog
        return "Success";
    }

        public function index(){
            //In this function we want to show the contents of our sesseion ---> and get the item details
            //We will create an array of items containing the data we need. So instead of doing our query in the 
            //frontend, we'll do it here in this function
            $item_cart = []; //initialize our empty array
            $total = 0;

            //checkk if we have a session--->cart
            if(Session::has('cart')){
                $cart = Session::get('cart');
                foreach($cart as $item_id => $cart_quantity){
                    //get the details of the item via id
                    $item = Item::find($item_id);
                    //attach the additional data that we need ie cart_quantity, subtotal
                    $item->cart_quantity = $cart_quantity;
                    $item->subtotal = $cart_quantity * $item->price;
                    //push the new item into our item_cart array
                    $item_cart[] = $item;

                    $total += $item->subtotal;
            }
            
        
            }

            return view('cart', compact('item_cart', 'total'));

        }

        public function removeFromCart($itemId){
            //we just want to remove the item in the session
            // Session::forget("cart.$itemId");
            Alert::html('Are you sure you want to remove this item?', "<a href='/remove-cart-item/$itemId' class='btn btn-danger'>Remove</a>", 'error');
            return back();
        }

        public function removeCartItem($itemId){
              Session::forget("cart.$itemId");
              return back();
        }

        public function destroy(){
            //we want to remove the cart from session
            Session::forget('cart');
            return back();
        }

        public function updateCartQty($itemId, Request $request){
            //get the cart from session
            //modify the quantity of the specific item
            //put the cart back in session
            $cart = Session::get('cart');
            $cart[$itemId] = $request->cart_quantity;
            Session::put('cart', $cart);
            return "Success";
        }
}

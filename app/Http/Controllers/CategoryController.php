<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();

        return view('categories', compact('categories'));
    }

    public function store(Request $request){

        $new_category = new Category;
        $new_category->name = $request->name;
        $new_category->save();

        return redirect('/categories');
    }

    public function destroy(Request $request){

        $id = $request->category_id;
        $category = Category::find($id);
        $category->delete();

        return redirect('/categories');
    }
}

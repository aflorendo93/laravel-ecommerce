<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Item::all();
        $categories = Category::all();

        //if we received something from the request, we need to get all the items with the corresponding category_id
        if($request->category_id){
            //we'll get the items with the category_id we received from the query string
            //where('column_name', search value)
            //get() -> will get all the rows that will satisfy the condition
            //first() -> will get the first row that will satisfy the condition
            $items = Item::where('category_id', $request->category_id)->get();
        }

        return view('items', compact('items'), compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('add-item', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
           "name" => "required",
           "description" => "required",
           "price" => "required|numeric",
           "quantity" => "required|numeric",
           "imgPath" => "required|image|mimes:jpeg,png,jpg,gif,svg,JPEG,PNG,JPG,GIF,SVG",
           "category_id" => "required"
       );

       //validate has 2 arguments, 1:request (data from form), 2: rules that the function will use to validate the form content
       $this->validate($request, $rules);

       $new_item = new Item;
       $new_item->name = $request->name;
       $new_item->description = $request->description;
       $new_item->price = $request->price;
       $new_item->quantity = $request->quantity;
       $new_item->category_id = $request->category_id;

       //A. Image Handling
       //a. Get the image from file
       $image = $request->file('imgPath');
       //b. rename the image
       $image_name = time() . "." . $image->getClientOriginalExtension();
       //c. assign the image destination
       $destination = "images/"; // this is in your public directory
       //d. move the image to its destination
       $image->move($destination, $image_name);
       //e. save the image path
    //    $new_item->imgPath = $destination . $image_name;
       //saving
       $result = \Cloudinary\Uploader::upload($destination . $image_name);


       $new_item->imgPath = $result['secure_url'];
       $new_item->save();

    //    $new_item->save();

       return redirect('/catalog');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $categories = Category::all();

        return view('update-item-form', compact('item'), compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
            $item = Item::find($id);
            $item->name = $request->name;
            $item->description = $request->description;
            $item->price = $request->price;
            $item->quantity = $request->quantity;
            $item->category_id = $request->category_id;
     
            $image = $request->file('imgPath');
            $image_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $item->imgPath = $destination . $image_name;
            $item->save();
            
            return redirect('/catalog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->item_id;
        $item = Item::find($id);
        $item->delete();

        return redirect('/catalog');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Item;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendInvoice;
use Session;

class OrderController extends Controller
{
    public function checkout(Request $request){
        //we'll save the order in our orders table
        //populate the item_order
        //we'll update the total in our order
        //return to cart


        $order = new Order;
        $order->total = $request->total;
        $order->status_id = 1; //pending
        $order->payment_id = $request->payment_id;
        $order->user_id = Auth::user()->id;
        $order->save();

        //Since item_order is many to many, we need to set up the relationship of the models involved.
        //get the cart from the session;
        $cart = Session::get('cart');
        //individually, attach the item id to the order esentially populating our item_order table
        //remember, the data in our $cart is an associative array.
        // $itemId=>$quantity
        foreach($cart as $item_id => $cart_quantity){
            //adding entry to our many to many table
            //$order is the recently saved order
            //remember that we have an extra column (quantity)
            $order->items()->attach($item_id, ["quantity" => $cart_quantity]);
            //item_order => order_id, item_id, quantity
             //we will update the quantity of the item.
            $item = Item::find($item_id);
            $item->quantity -= $cart_quantity;
            $item->save();
        } ;
        //normally you'll put here Auth::user()->email;
        Mail::to('aflorendo93@gmail.com')->send(new SendInvoice);
       //forget the session-cart
       Session::forget('cart');
       //redirect back to catalog
       return redirect('/catalog');

    }

    public function show(){
        //we want to get the orders of the logged in user.
        //SELECT * FROM orders WHERE==
        //where('column_name', 'value')
        $orders = Order::where("user_id", Auth::user()->id)->get();

        return view('user-transaction', compact('orders'));
    }


    public function index(){
        $orders = Order::all();
        
        return view('all-transactions', compact('orders'));
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    public function index(){
        $payments = Payment::all();

        return view('payments', compact('payments'));
    }

    public function store(Request $request){

        $new_payment = new Payment;
        $new_payment->name = $request->name;
        $new_payment->save();

        return redirect('/payments');
    }

    public function destroy(Request $request){

        $id = $request->payment_id;
        $payment = Payment::find($id);
        $payment->delete();

        return redirect('/payments');
    }
}

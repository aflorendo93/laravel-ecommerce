<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index(){
        $roles = Role::all();

        return view('roles', compact('roles'));
    }

    public function store(Request $request){

        $new_role = new Role;
        $new_role->name = $request->name;
        $new_role->save();

        return redirect('/roles');
    }

    public function destroy(Request $request){

        $id = $request->role_id;
        $role = Role::find($id);
        $role->delete();

        return redirect('/roles');
    }
}

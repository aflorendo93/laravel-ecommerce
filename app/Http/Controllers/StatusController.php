<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;

class StatusController extends Controller
{
    public function index(){
        $statuses = Status::all();

        return view('statuses', compact('statuses'));
    }

    public function store(Request $request){

        $new_status = new Status;
        $new_status->name = $request->name;
        $new_status->save();

        return redirect('/statuses');
    }

    public function destroy(Request $request){

        $id = $request->status_id;
        $status = Role::find($id);
        $status->delete();

        return redirect('/statuses');
    }
}

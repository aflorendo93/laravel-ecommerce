<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function items(){
        //If we have an additional column in our intermediary table
        //because remember, the default of an intermediary table is to have the foreign keys of the tables being joined
        return $this->belongsToMany('App\Item')->withPivot('quantity')->withTimeStamps();
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    public function payment(){
        return $this->belongsTo('App\Payment');
    }
}

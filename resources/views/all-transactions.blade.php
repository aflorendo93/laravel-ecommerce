@extends('layouts.template');
@section('title', 'Transactions Page')
@section('content')
    <h1 class="text-center py-3">Orders</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <table class="table table-striped border">
                    <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Order Date</th>
                            <th>User Details</th>
                            <th>Order Details</th>
                            <th>Total</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->created_at->diffForHumans()}}</td>
                                <td>{{$order->user->name}}
                                </td>
                                <td> 
                                    @foreach($order->items as $item)
                                        Name: {{$item->name}}, Quantity: {{$item->pivot->quantity}} <br>
                                    @endforeach
                                </td>
                                <td>{{$order->total}}</td>
                                <td>{{$order->payment->name}}</td>
                                <td>{{$order->status->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
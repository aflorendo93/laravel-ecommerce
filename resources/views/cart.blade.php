@extends('layouts.template')
@section('title', 'Cart')
@section('content')
    <script
        src="https://www.paypal.com/sdk/js?client-id=Afp3Ch-M3v_c7lcdvV5uFO8--6AHCA4ZjCF1SE26LzIUfYYscRtArhejISi6w29XBeTXHr4TVSh9onxk"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
    </script>
    <h1 class="text-center py-3">Cart</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <table class="table table-striped border">
                    <thead>
                        <tr>
                            <td>Item Name</td>
                            <td>Item Quantity</td>
                            <td>Item Price</td>
                            <td>Item Subtotal</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($item_cart as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>
                                <div class="input-group">
                                    <input type="number" name="cart_quantity" class="form-control" value="{{$item->cart_quantity}}" data-id="{{$item->id}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-success updateBtn">Update</button>
                                    </div>
                                </div>
                            </td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->subtotal}}</td>
                            <td><a href="/remove-from-cart/{{$item->id}}" class="btn btn-danger">Remove from Cart</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total: Php {{$total}}</td>
                            <td>
                                <a href="/empty-cart" class="btn btn-danger">Empty Cart</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <form action="/checkout" method="POST" id="paymentForm">
                                    @csrf
                                    <input type="hidden" name="payment_id" value="1">
                                    <input type="hidden" name="total" value="{{$total}}">
                                    <button class="btn btn-warning">Pay via Cash</button>
                                </form>
                            </td>
                            <td> <div id="paypal-button-container"></div></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript">
   
        const updateBtns = document.querySelectorAll('.updateBtn');
        updateBtns.forEach(function(updateBtn){
            updateBtn.addEventListener('click', function(e){
             
                const cart_quantity = e.target.parentElement.previousElementSibling.value;
                const item_id = e.target.parentElement.previousElementSibling.getAttribute('data-id');

                let data = new FormData;
                data.append("_token", "{{csrf_token ()}}")
                data.append("cart_quantity", cart_quantity);

                fetch("/update-cart/" + item_id, {
                    method: "POST",
                    
                    body: data
                }).then(function(response){
                    return response.text();
                }).then(function(data){
                    const price = e.target.parentElement.parentElement.parentElement.nextElementSibling.textContent;
                    const subtotal = parseInt(price) * parseInt(cart_quantity);
                    e.target.parentElement.parentElement.parentElement.nextElementSibling.nextElementSibling = subtotal;
                })
                
            })
        })
        //for paypal
        paypal.Buttons({
            createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{$total}}'
                    }
                    }]
                });
            },
            onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
                // This function shows a transaction success message to your buyer.
                const checkoutForm = document.getElementById('paymentForm');
                checkoutForm.firstElementChild.nextElementSibling.value = 3;
                checkoutForm.submit();
            });
            }
        }).render('#paypal-button-container');
    </script>

@endsection


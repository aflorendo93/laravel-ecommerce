@extends('layouts.template')
@section('title', 'Categories')
@section('content')
    <h1 class="py-5 text-center">Add Category</h1>
    <div class="container pt-1">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-category" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Category:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Add Category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-5 text-center">All Categories</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name}}</td>
                        <td>
                            <form action="/delete-category" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="category_id" value="{{$category->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection
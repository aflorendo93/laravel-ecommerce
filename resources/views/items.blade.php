@extends('layouts.template')
@section('title', 'Items')
@section('content')
    <h1 class="text-center py-3">Items</h1>
    @if(Session::has("message"))
        <h4 class="text-danger text-center">{{Session::get('message')}}</h4>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <h4>Filter by Category</h4>
                <div class="list-group">
                        <a href="/catalog">All</a>
                    @foreach($categories as $category)
                        <a href="/catalog?category_id={{$category->id}}" class="list-group-item list-group-item-action">{{$category->name}}</a>
                    @endforeach
                </div>
            </div>    
            <div class="col-lg-10">
                <div class="container">
                    <div class="row">
                        @foreach($items as $item)
                        <div class="col-lg-4">
                            <div class="card border border-light">
                                <img src="{{$item->imgPath}}" height="200px" alt="{{$item->name}}" class="card-img-top">
                                <div class="card-body">
                                    <h3 class="card-title">{{$item->name}}</h3>
                                    <p class="card-text">Description: {{$item->description}}</p>
                                    <p class="card-text">Price: {{$item->price}}</p>
                                    <p class="card-text">Quantity: {{$item->quantity}}</p>
                                    <p class="card-text">Category: {{$item->category->name}}</p>
                                </div>
                                @auth
                                    @if(Auth::user()->role_id === 2)
                                <div class="card-footer d-flex">
                                    <a href="/update-item/{{ $item->id }}" class="btn btn-info mr-5">Update</a>
                                    <form action="/delete-item" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="item_id" value="{{$item->id}}">
                                        <button type="submit" class="btn btn-danger">Delete</button>    
                                    </form>
                                </div>
                                    @else
                                <div class="card-footer">
                                    <!-- <form action="/add-to-cart/{{$item->id}}" method="post">
                                        @csrf -->
                                        <div class="input-group">
                                            <input type="number" name="cart_quantity" class="form-control" data-id="{{$item->id}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary addToCartBtn">Add to Cart</button>
                                            </div>
                                        </div>
                                    <!-- </form> -->
                                </div>
                                @endif
                                @endauth
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //capture the addToCartBtn
        //attach an event listener to each of the btn
        //capture the data
        //send the data via fetch
        const addToCartBtns = document.querySelectorAll('.addToCartBtn');
        addToCartBtns.forEach(function(addToCartBtn){
            addToCartBtn.addEventListener('click', function(e){
                //value of the input ---> cart_quantity
                //value of the item_id
                const cart_quantity = e.target.parentElement.previousElementSibling.value;
                const item_id = e.target.parentElement.previousElementSibling.getAttribute('data-id');

                let data = new FormData;
                data.append("_token", "{{csrf_token ()}}")
                data.append("cart_quantity", cart_quantity);

                fetch("/add-to-cart/" + item_id, {
                    method: "POST",
                    //we need the data from the form
                    body: data
                }).then(function(response){
                    return response.text();
                }).then(function(data){
                    console.log(data);
                })
                
            })
        })
    </script>
@endsection
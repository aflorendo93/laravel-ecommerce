
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- bootswatch -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.css">
    
</head>
<body>
@include('sweetalert::alert')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        @if (Auth::guest())
            <li class="nav-item active">
                <a class="nav-link" href="/catalog">Catalog<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <button class="btn btn-secondary">Login</button>
                </form>
            </li>
            <li class="nav-item">
                <form action="{{ route('register') }}" method="POST">
                        @csrf
                        <button class="btn btn-secondary">Register</button>
                </form>
            </li>
        @endif
            @auth
            <li class="nav-item active">
                <a class="nav-link" href="/catalog">Catalog<span class="sr-only">(current)</span></a>
            </li>
                @if(Auth::user()->role_id === 2)
            <li class="nav-item">
                <a class="nav-link" href="/transactions">All Orders</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/categories">Categories<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/payments">Payments</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/statuses">Statuses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/add-item">Add-Items</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/statuses">Statuses</a>
            </li>
                @else
            <li class="nav-item active">
                <a class="nav-link" href="/cart">Cart<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/orders">Orders<span class="sr-only">(current)</span></a>
            </li>
                @endif
            @endauth
            @auth
            <li class="nav-item">
                <a class="nav-link" href="#">Hi! {{Auth::user()->name}} </a>
            </li>
            <li class="nav-item">
                <form action="/logout" method="POST">
                        @csrf
                        <button class="btn btn-secondary">Logout</button>
                </form>
            @endauth
            </li>
        </ul>
    </div>
    </nav>

    @yield('content')

    <footer class="bg-primary">
        <div class="d-flex justify-content-center">
            <p class="my-5">Made with Love By A</p>
        </div>
        
    </footer>


</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table class="table table-striped border">
        <thead>
            <tr>
                <th>Order Id</th>
                <th>Order Date</th>
                <th>Order Details</th>
                <th>Total</th>
                <th>Payment</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->created_at->format('U')}}</td>
                    <td>{{$order->created_at->diffForHumans()}}</td>
                    <td>
                        @foreach($order->items as $item)
                            Name: {{$item->name}}, Quantity: {{$item->pivot->quantity}} <br>
                        @endforeach
                    </td>
                    <td>{{$order->total}}</td>
                    <td>{{$order->payment->name}}</td>
                    <td>{{$order->status->name}}</td>
                    <td></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
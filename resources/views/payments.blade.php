@extends('layouts.template')
@section('title', 'Payments')
@section('content')
    <h1 class="py-5 text-center">Add Payment</h1>
    <div class="container pt-1">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-payment" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Payment:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Add Payment</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-5 text-center">All Payments</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Payment</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{$payment->id}}</td>
                        <td>{{$payment->name}}</td>
                        <td>
                            <form action="/delete-payment" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="payment_id" value="{{$payment->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection
@extends('layouts.template')
@section('title', 'Roles')
@section('content')
    <h1 class="py-5 text-center">Add Role</h1>
    <div class="container pt-1">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-role" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Role:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Add Role</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-5 text-center">All Roles</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Roles</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->id}}</td>
                        <td>{{$role->name}}</td>
                        <td>
                            <form action="/delete-role" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="role_id" value="{{$role->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection
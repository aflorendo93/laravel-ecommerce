@extends('layouts.template')
@section('title', 'Statuses')
@section('content')
    <h1 class="py-5 text-center">Add Status</h1>
    <div class="container pt-1">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-status" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Status:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Add Status</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-5 text-center">All Statuses</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($statuses as $status)
                    <tr>
                        <td>{{$status->id}}</td>
                        <td>{{$status->name}}</td>
                        <td>
                            <form action="/delete-status" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="status_id" value="{{$status->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection
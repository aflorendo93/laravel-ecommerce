@extends('layouts.template')
@section('title', 'Update Item')
@section('content')
    <h1 class="text-center">Add Item Form</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/update-item/{{$item->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="name">Item Name</label>
                        <input type="text" name="name" value="{{$item->name}}"class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="description">Item Description</label>
                        <input type="text" name="description" value="{{$item->description}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="price">Item Price</label>
                        <input type="number" name="price" value="{{$item->price}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="quantity">Item Quantity</label>
                        <input type="number" name="quantity" value="{{$item->quantity}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="imgPath">Item Image</label>
                        <img src="/public/{{$item->imgPath}}" height="50px">
                        <input type="file" name="imgPath" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-info">Add Item</button>
                    </div>
                </form>
            
            </div>
        </div>
    </div>
@endsection
@extends('layouts.template');
@section('title', 'Orders Page')
@section('content')
    <h1 class="text-center py-3">Orders</h1>
    <div class="text-center">
        <a href="/download-orders" class="btn btn-warning">Download PDF</a>
        <a href="/download-orders-csv" class="btn btn-info">Download CSV</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <table class="table table-striped border">
                    <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Order Date</th>
                            <th>Order Details</th>
                            <th>Total</th>
                            <th>Payment</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->created_at->format('U')}}</td>
                                <td>{{$order->created_at->diffForHumans()}}</td>
                                <td>
                                    @foreach($order->items as $item)
                                        Name: {{$item->name}}, Quantity: {{$item->pivot->quantity}} <br>
                                    @endforeach
                                </td>
                                <td>{{$order->total}}</td>
                                <td>{{$order->payment->name}}</td>
                                <td>{{$order->status->name}}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware("admin") ->group(function(){
    Route::get('/roles', 'RoleController@index');
    Route::post('/add-role', 'RoleController@store');
    Route::delete('/delete-role', 'RoleController@destroy');
    
    Route::get('/categories', 'CategoryController@index');
    Route::post('/add-category', 'CategoryController@store');
    Route::delete('/delete-category', 'CategoryController@destroy');
    
    Route::get('/payments', 'PaymentController@index');
    Route::post('/add-payment', 'PaymentController@store');
    Route::delete('/delete-payment', 'PaymentController@destroy');
    
    Route::get('/statuses', 'StatusController@index');
    Route::post('/add-status', 'StatusController@store');
    Route::delete('/delete-status', 'StatusController@destroy');

    Route::get('/add-item', 'ItemController@create');
    Route::post('/add-item', 'ItemController@store');
    Route::delete('/delete-item', 'ItemController@destroy');
    Route::get('/update-item/{id}', 'ItemController@edit');
    Route::patch('/update-item/{id}', 'ItemController@update');

    Route::get('/transactions', 'OrderController@index');
});

Route::get('/catalog', 'ItemController@index');

Route::post('/add-to-cart/{item_id}', 'CartController@addToCart');
Route::get('/cart', 'CartController@index');
Route::get('/remove-from-cart/{itemId}', 'CartController@removeFromCart');
Route::get('/remove-cart-item/{itemId}', 'CartController@removeCartItem');

Route::get('/empty-cart', 'CartController@destroy');
Route::post('/update-cart/{itemId}', 'CartController@updateCartQty');

Route::get('download-orders', 'AddonController@downloadOrders');
Route::get('download-orders-csv', 'AddonController@downloadOrdersCsv');

Route::post('/checkout', 'OrderController@checkout');
Route::get('/orders', 'OrderController@show');

